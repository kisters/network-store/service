from pathlib import Path

from setuptools import find_namespace_packages, setup

with open(Path(__file__).resolve().parent / "README.md") as fh:
    long_description = fh.read()

setup(
    name="kisters.network_store.service",
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description="Storage, Retrieval, and Versioning of Infrastructure Networks",
    long_description=long_description,
    url="https://gitlab.com/kisters/network-store/service",
    author="Jesse VanderWees",
    author_email="jesse.vanderwees@kisters-bv.nl",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
    ],
    keywords="hydraulics infrastructure graph topology",
    packages=find_namespace_packages(include=["kisters.*"]),
    include_package_data=True,
    license_files=("AGPLv3-LICENSE",),
    zip_safe=False,
    install_requires=[
        "bokeh",
        "fastapi",
        "kisters.network_store.model_library>=0.2.9",
        # KISTERS access-control is not open-source (yet)
        # "kisters.water.operational.access-control",
        "motor",
        "numpy",
        "orjson",
        "pydantic[dotenv]",
        "pymongo",
        "scipy",
        "starlette",
    ],
    extras_require={
        "test": [
            "pytest",
            "pytest-cov",
            "pytest-asyncio",
            "kisters.network_store.model_library.water>=0.2.13",
            "httpx",
        ],
    },
)
