import datetime
import uuid

import pytest
from kisters.network_store.model_library.base import LocationSet

from kisters.network_store.service.sadp.mongodb.network_store import NetworkStore


@pytest.fixture
async def network_store_and_network_id(event_loop):
    network_id = f"test_{str(uuid.uuid4())[:8]}"
    network_store = NetworkStore(event_loop=event_loop)
    yield network_store, network_id
    await network_store.drop_network(network_id)
    network_store._client.close()


@pytest.fixture
def nodes():
    return [
        {
            "uid": "junction",
            "location": {"x": 1.5, "y": 2.0, "z": 3.0},
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        },
        {
            "uid": "reservoir",
            "location": {"x": 1.0, "y": 2.0, "z": 3.0},
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        },
        {
            "uid": "tank",
            "location": {"x": 2.0, "y": 2.0, "z": 5.0},
            "level_volume": [
                {"level": 4.0, "volume": 0.0},
                {"level": 10.0, "volume": 100.0},
            ],
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        },
    ]


@pytest.fixture
def links():
    return [
        {
            "uid": "channel",
            "source_uid": "upstream_node",
            "target_uid": "downstream_node",
            "length": 100.0,
            "roughness": 10.0,
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        },
        {
            "uid": "pipe",
            "source_uid": "upstream_node",
            "target_uid": "downstream_node",
            "diameter": 1.0,
            "length": 10.0,
            "roughness": 10.0,
            "model": "hazen-williams",
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        },
        {
            "uid": "pump",
            "source_uid": "upstream_node",
            "target_uid": "downstream_node",
            "frequency": [
                {"flow": 1, "head": 1, "frequency": 1},
                {"flow": 3, "head": 3, "frequency": 1},
            ],
            "min_frequency": 1.0,
            "max_frequency": 1.0,
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        },
        {
            "uid": "valve",
            "source_uid": "upstream_node",
            "target_uid": "downstream_node",
            "diameter": 10.0,
            "model": "prv",
            "coefficient": 1.0,
            "setting": 0.0,
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        },
    ]


@pytest.fixture
def extents_nodes():
    nodes = [
        {
            "uid": f"reservoir{i}",
            "location": {"x": i, "y": i, "z": -i},
            "schematic_location": {"x": i, "y": i, "z": -i},
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        }
        for i in range(1, 6)
    ]
    # Add a node with a deleted date
    nodes.append(
        {
            "uid": "reservoir6",
            "location": {"x": 6, "y": 6, "z": -6},
            "schematic_location": {"x": 6, "y": 6, "z": -6},
            "created": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=1,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
            "deleted": datetime.datetime(
                year=2019,
                month=1,
                day=1,
                hour=2,
                minute=0,
                second=0,
                tzinfo=datetime.timezone.utc,
            ),
        }
    )
    return nodes


async def test_nodes(network_store_and_network_id, nodes):
    network_store, network_id = network_store_and_network_id

    # Create
    input_nodes = sorted(nodes, key=lambda n: n["uid"])
    await network_store.save_nodes(network_id, input_nodes)

    # Read
    output_nodes = await network_store.get_nodes(network_id)
    output_nodes = sorted(output_nodes, key=lambda n: n["uid"])

    for a, b in zip(input_nodes, output_nodes):
        a.pop("_id")
        assert a == b

    # Delete: soft
    deleted_dt = network_store.make_datetime()
    await network_store.drop_nodes(
        network_id, uids=[n["uid"] for n in nodes], dt=deleted_dt
    )
    output_nodes = await network_store.get_nodes(network_id)
    assert output_nodes == []

    # Read: go back in time
    dt = deleted_dt - datetime.timedelta(milliseconds=1)
    output_nodes = await network_store.get_nodes(network_id, dt=dt)
    output_nodes = sorted(output_nodes, key=lambda n: n["uid"])
    for a, b in zip(input_nodes, output_nodes):
        assert b["deleted"] == deleted_dt
        b.pop("deleted")
        assert a == b

    # Delete: purge
    await network_store.drop_nodes(
        network_id, uids=[n["uid"] for n in nodes], purge=True
    )
    output_nodes = await network_store.get_nodes(network_id, dt=dt)
    assert output_nodes == []


async def test_links(network_store_and_network_id, links):
    network_store, network_id = network_store_and_network_id

    # Create
    input_links = sorted(links, key=lambda n: n["uid"])
    await network_store.save_links(network_id, input_links)

    # Read
    output_links = await network_store.get_links(network_id)
    output_links = sorted(output_links, key=lambda n: n["uid"])

    for a, b in zip(input_links, output_links):
        a.pop("_id", None)
        assert a == b

    # Delete: soft
    deleted_dt = network_store.make_datetime()
    await network_store.drop_links(
        network_id, uids=[n["uid"] for n in links], dt=deleted_dt
    )
    output_links = await network_store.get_links(network_id)
    assert output_links == []

    # Read: go back in time
    dt = deleted_dt - datetime.timedelta(milliseconds=1)
    output_links = await network_store.get_links(network_id, dt=dt)
    output_links = sorted(output_links, key=lambda n: n["uid"])
    for a, b in zip(input_links, output_links):
        assert b["deleted"] == deleted_dt
        b.pop("deleted")
        assert a == b

    # Delete: purge
    await network_store.drop_links(
        network_id, uids=[n["uid"] for n in links], purge=True
    )
    output_links = await network_store.get_links(network_id, dt=dt)
    assert output_links == []


async def test_get_extent(network_store_and_network_id, extents_nodes):
    network_store, network_id = network_store_and_network_id
    await network_store.save_nodes(network_id, extents_nodes)

    extent = await network_store.get_extent(
        network_id,
        location_set=LocationSet.SCHEMATIC,
        dt=datetime.datetime(
            year=2019,
            month=1,
            day=1,
            hour=1,
            minute=0,
            second=0,
            tzinfo=datetime.timezone.utc,
        ),
    )
    assert extent == {"x": [1, 6], "y": [1, 6], "z": [-6, -1]}

    extent = await network_store.get_extent(
        network_id,
        location_set=LocationSet.GEOGRAPHIC,
        dt=datetime.datetime(
            year=2019,
            month=1,
            day=1,
            hour=1,
            minute=0,
            second=0,
            tzinfo=datetime.timezone.utc,
        ),
    )
    assert extent == {"x": [1, 6], "y": [1, 6], "z": [-6, -1]}

    extent = await network_store.get_extent(
        network_id,
        location_set=LocationSet.SCHEMATIC,
        dt=datetime.datetime(
            year=2019,
            month=1,
            day=1,
            hour=2,
            minute=0,
            second=0,
            tzinfo=datetime.timezone.utc,
        ),
    )
    assert extent == {"x": [1, 5], "y": [1, 5], "z": [-5, -1]}

    extent = await network_store.get_extent(
        network_id,
        location_set=LocationSet.GEOGRAPHIC,
        dt=datetime.datetime(
            year=2019,
            month=1,
            day=1,
            hour=2,
            minute=0,
            second=0,
            tzinfo=datetime.timezone.utc,
        ),
    )
    assert extent == {"x": [1, 5], "y": [1, 5], "z": [-5, -1]}


async def test_duplicate_uids(network_store_and_network_id, links, nodes):
    network_store, network_id = network_store_and_network_id
    with pytest.raises(ValueError):
        await network_store.save_nodes(network_id, [nodes[0], nodes[0]])
    with pytest.raises(ValueError):
        await network_store.save_links(network_id, [links[0], links[0]])


@pytest.mark.parametrize("limit", [2, 3, None])
async def test_get_uid_chunking(network_store_and_network_id, links, nodes, limit):
    network_store, network_id = network_store_and_network_id
    await network_store.save_links(network_id, links)
    await network_store.save_nodes(network_id, nodes)

    chunk_uids = await network_store.get_links(
        network_id, start_uid_chunks=True, limit=limit
    )
    expected = [e["uid"] for e in links][:: (limit or 1000)]
    assert expected == chunk_uids
    elements = []
    for uid in chunk_uids:
        _elems = await network_store.get_links(network_id, limit=limit, start_uid=uid)
        elements.extend(_elems)
    for a, b in zip(links, elements):
        a.pop("_id", None)
        assert a == b

    chunk_uids = await network_store.get_nodes(
        network_id, start_uid_chunks=True, limit=limit
    )
    expected = [e["uid"] for e in nodes][:: (limit or 1000)]
    assert expected == chunk_uids
    elements = []
    for uid in chunk_uids:
        _elems = await network_store.get_nodes(network_id, limit=limit, start_uid=uid)
        elements.extend(_elems)
    for a, b in zip(nodes, elements):
        a.pop("_id", None)
        assert a == b
