import uuid
from typing import Tuple

import pytest
from fastapi.encoders import jsonable_encoder
from httpx import AsyncClient
from kisters.network_store.model_library.water.groups import Subsystem
from kisters.network_store.model_library.water.links import (
    Delay,
    Drain,
    FlowControlledStructure,
)
from kisters.network_store.model_library.water.nodes import (
    FlowBoundary,
    LevelBoundary,
    Storage,
)

from kisters.network_store.service.padp.asgi import create_app
from kisters.network_store.service.sadp.mongodb import NetworkStore

API_PATH = "/network-store"


@pytest.fixture
async def test_client(event_loop):
    network_url = f"{API_PATH}/networks/recursive-groups-{str(uuid.uuid4())[:8]}"
    async with AsyncClient(
        app=create_app(NetworkStore(event_loop=event_loop), api_path=API_PATH),
        base_url="http://testserver",
    ) as client:
        yield client, network_url
        await client.delete(network_url, params={"purge": True})


def construct_test_group() -> Tuple[str, dict]:
    group_uid = f"group{str(uuid.uuid4())[:8]}"
    group = Subsystem(uid=group_uid, location={"x": 0.0, "y": 0.0})
    nodes = [
        Storage(
            uid=f"{group_uid}_storage",
            group_uid=group_uid,
            location={"x": 0.0, "y": 0.0},
            level_volume=[{"level": 0, "volume": 0}, {"level": 1, "volume": 1}],
        ),
        FlowBoundary(
            uid=f"{group_uid}_upstream_node",
            group_uid=group_uid,
            location={"x": 0.0, "y": 0.0},
        ),
        LevelBoundary(
            uid=f"{group_uid}_downstream_node_1",
            group_uid=group_uid,
            location={"x": 0.0, "y": 0.0},
        ),
        LevelBoundary(
            uid=f"{group_uid}_downstream_node_2",
            group_uid=group_uid,
            location={"x": 0.0, "y": 0.0},
        ),
    ]
    links = [
        Delay(
            uid=f"{group_uid}_delay",
            group_uid=group_uid,
            source_uid=f"{group_uid}_upstream_node",
            target_uid=f"{group_uid}_storage",
            transit_time=0.0,
        ),
        Drain(
            uid=f"{group_uid}_drain",
            group_uid=group_uid,
            source_uid=f"{group_uid}_storage",
            target_uid=f"{group_uid}_downstream_node_1",
            level=0.0,
            min_area=0.0,
            max_area=1.0,
            flow_model="free",
            coefficient=1.0,
        ),
        FlowControlledStructure(
            uid=f"{group_uid}_overflow",
            group_uid=group_uid,
            source_uid=f"{group_uid}_storage",
            target_uid=f"{group_uid}_downstream_node_2",
            min_flow=0.0,
            max_flow=1.0,
        ),
    ]
    payload = jsonable_encoder(
        {"groups": [group], "links": links, "nodes": nodes}, exclude_none=True
    )
    return group.uid, payload


def flatten_uids(payload: dict) -> dict:
    result = {"groups": [], "links": [], "nodes": []}
    for c in ("links", "nodes", "groups"):
        result[c].extend(e["uid"] for e in payload.get(c, ()))
        result[c].sort()
    return result


def merge_networks(*payloads: dict) -> dict:
    result = {"groups": [], "links": [], "nodes": []}
    for p in payloads:
        for c in ("links", "nodes", "groups"):
            result[c].extend(p.get(c, ()))
        result[c].sort(key=lambda x: x["uid"])
    return result


async def get_by_group_uid(
    group_uid: str,
    client: AsyncClient,
    network: str,
    recursive: bool = True,
    include_parent_group: bool = False,
):
    result = {}
    for c in ("groups", "links", "nodes"):
        res = await client.post(
            f"{network}/{c}/search",
            params={"include_subgroups": recursive},
            json={"group_uids": [group_uid]},
        )
        res.raise_for_status()
        result[c] = res.json()
    if include_parent_group:
        res = await client.post(f"{network}/groups/search", json={"uids": [group_uid]})
        res.raise_for_status()
        result["groups"].extend(res.json())
    return result


async def test_get_by_group(test_client: Tuple[AsyncClient, str]):
    test_client, network = test_client

    group_uid, payload = construct_test_group()
    res = await test_client.post(network, json=payload)
    res.raise_for_status()

    orig = flatten_uids(payload)
    remote = await get_by_group_uid(
        group_uid, test_client, network, include_parent_group=True
    )
    remote = flatten_uids(remote)
    assert orig == remote


@pytest.mark.parametrize("depth", [1, 5])
async def test_get_by_group_binary_nested(
    test_client: Tuple[AsyncClient, str], depth: int
):
    test_client, network = test_client

    sub_groups = {}
    aggregated_groups = {}
    previous_parent_group_uid = None

    # a binary tree of groups
    for _ in range(depth):
        # New parent group "node"
        parent_group_uid, parent_group = construct_test_group()
        parent_group = {"groups": parent_group["groups"]}

        # New test group "branch"
        group_uid, payload = construct_test_group()
        payload["groups"][0]["group_uid"] = parent_group_uid
        sub_groups[group_uid] = payload

        # Construct aggregated group. All existing groups are sub_groups of
        # current parent group
        aggregated_groups[parent_group_uid] = merge_networks(*sub_groups.values())
        sub_groups[parent_group_uid] = parent_group

        # parent groups are not members of their own group
        if previous_parent_group_uid:
            previous_parent_group = sub_groups[previous_parent_group_uid]
            previous_parent_group["groups"][0]["group_uid"] = parent_group_uid
        previous_parent_group_uid = parent_group_uid

    # Store sub_groups
    for g in sub_groups.values():
        res = await test_client.post(
            network, params={"purge": False, "delete_existing": False}, json=g
        )
        res.raise_for_status()

    # Get strictly by group_uid
    for orig in sub_groups.values():
        assert len(orig["groups"]) == 1
        group_uid = orig["groups"][0]["uid"]
        if group_uid in aggregated_groups:
            continue
        remote = await get_by_group_uid(
            group_uid, test_client, network, recursive=False
        )
        assert flatten_uids(
            {"links": orig["links"], "nodes": orig["nodes"]}
        ) == flatten_uids(remote)

    # Get by group_uid, but also include subgroups
    for group_uid, orig in aggregated_groups.items():
        remote = await get_by_group_uid(group_uid, test_client, network)
        assert flatten_uids(orig) == flatten_uids(remote)
