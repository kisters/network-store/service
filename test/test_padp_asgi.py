import itertools
import uuid

import pytest
from fastapi.encoders import jsonable_encoder
from httpx import AsyncClient
from kisters.network_store.model_library.base import LocationSet
from kisters.network_store.model_library.water import groups, links, nodes

from kisters.network_store.service.padp.asgi import create_app
from kisters.network_store.service.sadp.mongodb import NetworkStore

API_PATH = "/network-store"


@pytest.fixture
async def test_client(event_loop):
    network_url = f"{API_PATH}/networks/test_{str(uuid.uuid4())[:8]}"
    async with AsyncClient(
        app=create_app(NetworkStore(event_loop=event_loop), api_path=API_PATH),
        base_url="http://testserver",
    ) as client:
        yield client, network_url
        await client.delete(network_url, params={"purge": True})


async def test_get_docs(test_client):
    test_client, _ = test_client
    res = await test_client.get(f"{API_PATH}/docs")
    res.raise_for_status()


async def test_get_openapi_json(test_client):
    test_client, _ = test_client
    res = await test_client.get(f"{API_PATH}/openapi.json")
    res.raise_for_status()


async def test_get_schema(test_client):
    test_client, _ = test_client
    res = await test_client.get(f"{API_PATH}/schemas/water/nodes/Storage")
    res.raise_for_status()
    assert res.json()
    res = await test_client.get(f"{API_PATH}/schemas/water/nodes/SwimmingHole")
    assert res.status_code == 404


async def test_redirect(test_client):
    test_client, _ = test_client

    res = await test_client.get("")
    assert res.is_redirect
    assert res.has_redirect_location

    res = await test_client.get("/")
    assert res.is_redirect
    assert res.has_redirect_location

    res = await test_client.get(API_PATH)
    assert res.is_redirect
    assert res.has_redirect_location

    res = await test_client.get(f"{API_PATH}/")
    assert res.is_redirect
    assert res.has_redirect_location


@pytest.fixture
async def initialized_test_client(test_client):
    test_client, network_url = test_client
    payload = jsonable_encoder(
        {"links": test_links, "nodes": test_nodes, "groups": test_groups},
        exclude_none=True,
    )
    res = await test_client.post(
        network_url, json=payload, params={"delete_existing": True, "purge": True}
    )
    res.raise_for_status()
    yield test_client, network_url


test_links = [
    links.Channel(
        uid="channel",
        source_uid="junction",
        target_uid="storage",
        length=100.0,
        hydraulic_routing={
            "model": "saint-venant",
            "stations": {
                "roughness": 10.0,
                "cross_section": [
                    {"z": 10, "lr": -5},
                    {"z": 0, "lr": -5},
                    {"z": 0, "lr": 5},
                    {"z": 10, "lr": 5},
                ],
            },
        },
    ),
    links.Delay(
        uid="delay", source_uid="junction", target_uid="storage", transit_time=10.0
    ),
    links.FlowControlledStructure(
        uid="flow_controlled_structure",
        source_uid="junction",
        target_uid="storage",
        min_flow=-1.0,
        max_flow=1.0,
    ),
    links.Pipe(
        uid="pipe",
        source_uid="junction",
        target_uid="storage",
        diameter=1.0,
        length=10.0,
        roughness=10.0,
        model="hazen-williams",
    ),
    links.Valve(
        uid="valve",
        source_uid="junction",
        target_uid="storage",
        diameter=10.0,
        model="prv",
        coefficient=1.0,
        setting=0.0,
    ),
]
test_nodes = [
    nodes.FlowBoundary(uid="flow_boundary", location={"x": 0.0, "y": 0.0, "z": 0.0}),
    nodes.Junction(uid="junction", location={"x": 0.0, "y": 1.0, "z": 0.0}),
    nodes.LevelBoundary(uid="level_boundary", location={"x": 1.0, "y": 0.0, "z": 0.0}),
    nodes.Storage(
        uid="storage",
        location={"x": 1.0, "y": 1.0, "z": 0.0},
        level_volume=[{"level": 0.0, "volume": 0.0}, {"level": 10.0, "volume": 10.0}],
    ),
]
test_groups = [
    groups.Subsystem(uid="subsystem", location={"x": 0.0, "y": 0.0}),
    groups.PumpingStation(uid="pumping_station", location={"x": 0.0, "y": 0.0}),
]


async def test_create_network(test_client):
    test_client, network_url = test_client
    payload = jsonable_encoder(
        {"links": test_links, "nodes": test_nodes, "groups": test_groups},
        exclude_none=True,
    )
    res = await test_client.post(
        network_url, json=payload, params={"delete_existing": True, "purge": True}
    )
    res.raise_for_status()


async def test_get_networks(initialized_test_client):
    test_client, network_url = initialized_test_client
    url, network_name = network_url.rsplit("/", maxsplit=1)
    res = await test_client.get(url)
    res.raise_for_status()
    names = res.json()
    assert network_name in names


link_loc_attr_map = {
    LocationSet.SCHEMATIC: "schematic_vertices",
    LocationSet.GEOGRAPHIC: "vertices",
}


@pytest.mark.parametrize("location_set", [v.value for v in LocationSet] + [None])
async def test_get_topology(initialized_test_client, location_set):
    test_client, network_url = initialized_test_client

    params = {"only_topology": True}
    if location_set:
        params["location_set"] = location_set
    else:
        # None implies default
        location_set = LocationSet.GEOGRAPHIC
    res = await test_client.post(f"{network_url}/nodes/search", params=params)
    res.raise_for_status()
    nodes_topology = res.json()
    sorted_nodes = sorted(test_nodes, key=lambda n: n.uid)
    sorted_topology_nodes = sorted(nodes_topology, key=lambda n: n["uid"])
    assert len(sorted_topology_nodes) == len(sorted_nodes)
    for a, b in zip(sorted_nodes, sorted_topology_nodes):
        a = a.dict()
        assert a["uid"] == b["uid"]
        assert a[location_set] == b[location_set]
    res = await test_client.post(f"{network_url}/links/search", params=params)
    res.raise_for_status()
    links_topology = res.json()
    sorted_links = sorted(test_links, key=lambda n: n.uid)
    sorted_topology_links = sorted(links_topology, key=lambda n: n["uid"])
    assert len(sorted_topology_links) == len(sorted_links)
    for a, b in zip(sorted_links, sorted_topology_links):
        a = a.dict()
        assert a["uid"] == b["uid"]
        prop = link_loc_attr_map[location_set]
        assert a.get(prop) == b.get(prop)


async def test_get_extent(initialized_test_client):
    test_client, network_url = initialized_test_client
    res = await test_client.get(f"{network_url}/extent")
    res.raise_for_status()
    extent = res.json()
    assert extent == {"x": [0, 1], "y": [0, 1], "z": [0, 0]}
    res = await test_client.get(
        f"{network_url}/extent", params={"location_set": "schematic_location"}
    )
    res.raise_for_status()
    schematic_extent = res.json()
    assert schematic_extent == {"x": [0, 1], "y": [0, 1], "z": [0, 0]}


@pytest.mark.parametrize(
    "kind",
    [("links", test_links), ("nodes", test_nodes), ("groups", test_groups)],
    ids=lambda x: x[0],
)
async def test_get_by_kind(initialized_test_client, kind):
    test_client, network_url = initialized_test_client
    res = await test_client.post(f"{network_url}/{kind[0]}/search")
    res.raise_for_status()
    local = sorted(kind[1], key=lambda n: n.uid)
    remote = sorted(res.json(), key=lambda n: n["uid"])
    assert len(remote) == len(local)
    for a, b in zip(local, remote):
        a = a.dict(exclude_none=True)
        assert b["created"] is not None
        del b["created"]
        assert a == b


@pytest.mark.parametrize(
    "kind",
    [("links", test_links), ("nodes", test_nodes), ("groups", test_groups)],
    ids=lambda x: x[0],
)
async def test_get_by_element_class(initialized_test_client, kind):
    test_client, network_url = initialized_test_client
    kind, elements = kind

    resall = await test_client.post(f"{network_url}/{kind}/search")
    resall.raise_for_status()
    for e in elements:
        res = await test_client.post(
            f"{network_url}/{kind}/search", params={"element_class": e.element_class}
        )
        res.raise_for_status()
        a = e.dict(exclude_none=True)
        remote_elements = res.json()
        assert len(remote_elements) == 1
        [b] = remote_elements
        del b["created"]
        assert a == b


@pytest.mark.parametrize(
    "elements",
    [("links", test_links), ("nodes", test_nodes), ("groups", test_groups)],
    ids=lambda x: x[0],
)
async def test_element_crd_query(test_client, elements):
    test_client, network_url = test_client
    kind, elements = elements

    res = await test_client.post(
        f"{network_url}/{kind}",
        json={"elements": jsonable_encoder(elements, exclude_none=True)},
    )
    res.raise_for_status()

    res = await test_client.post(f"{network_url}/{kind}/search")
    res.raise_for_status()

    elements = sorted(elements, key=lambda n: n.uid)
    remote_elements = res.json()
    remote_elements = sorted(remote_elements, key=lambda n: n["uid"])
    assert len(elements) == len(remote_elements)
    for a, b in zip(elements, remote_elements):
        assert b["created"] is not None
        del b["created"]
        assert a.dict(exclude_none=True) == b

    for i, e in enumerate(elements):
        res = await test_client.delete(f"{network_url}/{kind}", params={"uid": e.uid})
        res.raise_for_status()
        res = await test_client.post(f"{network_url}/{kind}/search")
        res.raise_for_status()
        found = res.json()
        if i != len(elements) - 1:
            assert found
            assert e.uid not in {ee["uid"] for ee in found}
        else:
            assert not found


@pytest.mark.parametrize(
    "elements",
    [("links", test_links), ("nodes", test_nodes), ("groups", test_groups)],
    ids=lambda x: x[0],
)
async def test_element_crd_body(test_client, elements):
    test_client, network_url = test_client
    kind, elements = elements

    res = await test_client.post(
        f"{network_url}/{kind}",
        json={"elements": jsonable_encoder(elements, exclude_none=True)},
    )
    res.raise_for_status()

    res = await test_client.post(f"{network_url}/{kind}/search")
    res.raise_for_status()

    elements = sorted(elements, key=lambda n: n.uid)
    remote_elements = res.json()
    remote_elements = sorted(remote_elements, key=lambda n: n["uid"])
    assert len(elements) == len(remote_elements)
    for a, b in zip(elements, remote_elements):
        assert b["created"] is not None
        del b["created"]
        assert a.dict(exclude_none=True) == b

    for i, e in enumerate(elements):
        res = await test_client.request(
            "DELETE", f"{network_url}/{kind}", json={"uids": [e.uid]}
        )
        res.raise_for_status()
        res = await test_client.post(f"{network_url}/{kind}/search")
        res.raise_for_status()
        found = res.json()
        if i != len(elements) - 1:
            assert found
            assert e.uid not in {ee["uid"] for ee in found}
        else:
            assert not found


async def test_duplicate_uids(test_client):
    test_client, network_url = test_client
    links = [test_links[0], test_links[0]]
    nodes = [test_nodes[0], test_nodes[0]]
    payload = jsonable_encoder({"links": links, "nodes": nodes}, exclude_none=True)
    with pytest.raises(Exception):
        res = await test_client.post(network_url, json=payload)
        res.raise_for_status()
    with pytest.raises(Exception):
        res = await test_client.post(network_url + "/nodes", json={"elements": nodes})
        res.raise_for_status()
    with pytest.raises(Exception):
        res = await test_client.post(network_url + "/links", json={"elements": links})
        res.raise_for_status()


@pytest.mark.parametrize("skip", [0, 2])
@pytest.mark.parametrize("limit", [100, 4])
@pytest.mark.parametrize("elements", [("links", test_links), ("nodes", test_nodes)])
async def test_skip_limit(initialized_test_client, elements, skip, limit):
    test_client, network_url = initialized_test_client
    kind, elements = elements

    res = await test_client.post(
        f"{network_url}/{kind}/search", params={"skip": skip, "limit": limit}
    )
    res.raise_for_status()

    local_elements = [e.uid for e in elements]
    expected = list(itertools.islice(local_elements, skip, skip + limit))
    remote_elements = [e["uid"] for e in res.json()]
    assert expected == remote_elements


@pytest.mark.parametrize("elements", [("links", test_links), ("nodes", test_nodes)])
async def test_start_uid(initialized_test_client, elements):
    test_client, network_url = initialized_test_client
    kind, elements = elements

    start_uid = elements[len(elements) // 2].uid
    res = await test_client.post(
        f"{network_url}/{kind}/search", params={"start_uid": start_uid}
    )
    res.raise_for_status()

    local_elements = [e.uid for e in elements if e.uid >= start_uid]
    remote_elements = [e["uid"] for e in res.json()]
    assert local_elements == remote_elements
