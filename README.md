# Network Store Service

## Overview

This package implements a hydraulic network topology store by mapping
structures such as tanks, reservoirs, and junctions onto the nodes of a graph,
whereas structures such as channels, pipes, pumps, and valves are mapped onto
graph edges.

The structure models are implemented as separate, open-source libraries:

- [`kisters.network_store.model_library`](https://gitlab.com/kisters/network-store/model-library):
  Base Classes for Network Models
- [`kisters.network_store.model_library.water`](https://gitlab.com/kisters/network-store/model-library-water):
  Network Models for Water Infrastructure

Furthermore, KISTERS maintains a python client for easy scripting against the
Network Store Service:
[`kisters.network_store.client.network`](https://gitlab.com/kisters/network-store/client)

## Licence

The KISTERS Network Store Service (package name
`kisters.network-store.service`) is distributed under the GNU Affero General
Public License v3.0 (AGPLv3), with commercial licences available. See the
[AGPLv3-LICENCE](AGPLv3-LICENCE) file for details about the AGPLv3 licence. To
receive a commercial licence, contact [KISTERS](https://www.kisters.de/).

## Running

The service can be run locally using uvicorn (installable with pip):

```shell
uvicorn kisters.network_store.service.padp.asgi.app:create_app --factory
```

### Environment Variables

| Variable                | Default | Description                                              |
| ----------------------- | ------- | -------------------------------------------------------- |
| `DEPLOYMENT_URL`        | -       | The public url that the ASGI service is exposed at       |
| `MONGODB_HOST`          | -       | The MongoDB connection URI for the storage backend       |
| `ENABLE_VIEWER`         | `True`  | Enable experimental network viewer feature in Swagger UI |
| `ENABLE_ACCESS_CONTROL` | `False` | Enable access control on all endpoints                   |


## Versioning

The package versioning attempts to follow semantic versioning. If the API is
extended, a minor version will be released. If there is a breaking change to
the API, the major version will be increased. Note that the modules exported by
the python package are considered internal, the versions are based on the ASGI
interface alone.
